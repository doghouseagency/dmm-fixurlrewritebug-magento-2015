# Doghouse_FixUrlRewriteBug

There are three issues with the core url rewrite table in Magento:

 - Reindex will add a new entry for products with duplicate url_key's - every single time
 - Disabled products clutter the table
 - Products that are not-visible individually clutter the table

This module only fixes the first bug - it's the most important one. It will eventually make the table grow the millions of rows and hundreds of MB's.

Tested with Magento 1.9.2.1, but apparently it works for 1.7.x to 1.9.x. Make sure to test it!

The fix is taken from the following gist: https://gist.github.com/edannenberg/5310008

Which was created for the following Github issue: https://github.com/magento/bugathon_march_2013/issues/265

## core_url_rewrite table is too big!

If you're installing this, it means the table has grown too big.

First make sure you don't have any duplicate URL keys. You can find out how many duplicate url key's you ahve with the following query:

    SELECT COUNT(DISTINCT entity_id) AS amount, `value`
    FROM catalog_product_entity_varchar v
    WHERE EXISTS (
        SELECT *
        FROM eav_attribute a
        WHERE attribute_code = "url_key"
        AND v.attribute_id = a.attribute_id
        AND EXISTS (
            SELECT *
            FROM eav_entity_type e
            WHERE entity_type_code = "catalog_product"
            AND a.entity_type_id = e.entity_type_id
        )
    )
    GROUP BY v.VALUE
    ORDER BY `value` DESC;

If you don't want to remove those duplicate keys, you can just install this module and truncate the core_url_rewrite table and reindex (after you've created a back-up of course).

## Installation

Install with Composer. Truncate table and reindex. Make sure you don't remove the URL rewrites that were created by a person by instead of truncating, doing something like:

    DELETE
    FROM core_url_rewrite
    WHERE is_system <> 1
    AND id_path REGEXP "^[0-9]+_[0-9]+$";

Before doing that, make sure you have a grip on your SEO situation since it might make existing links on the web useless.
